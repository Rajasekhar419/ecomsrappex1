package com.app.raja.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.raja.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Long>{

}
