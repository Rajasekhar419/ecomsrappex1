package com.app.raja.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.raja.entity.Category;
import com.app.raja.repo.CategoryRepo;
import com.app.raja.service.ICategoryService;

@Service
public class CategoryServiceImpl implements ICategoryService{
	@Autowired
	private CategoryRepo repo;

	@Override
	@Transactional
	public Long saveCategory(Category cat) {
		// TODO Auto-generated method stub
		return repo.save(cat).getId();
	}

	@Override
	@Transactional
	public void updateCategory(Category cat) {
        repo.save(cat);
		
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Category getOneCategory(Long id) {
		// TODO Auto-generated method stub
		
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly=true)
	public List<Category> getAllCategorys() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	
	

}
