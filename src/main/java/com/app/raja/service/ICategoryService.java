package com.app.raja.service;

import java.util.List;

import com.app.raja.entity.Category;

public interface ICategoryService {
	Long saveCategory(Category cat);
	void updateCategory(Category cat);
	void deleteCategory(Long id);
	Category getOneCategory(Long id);
	List<Category> getAllCategorys();

}
