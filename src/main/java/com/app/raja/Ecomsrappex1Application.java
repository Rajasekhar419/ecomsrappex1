package com.app.raja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ecomsrappex1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ecomsrappex1Application.class, args);
	}

}
