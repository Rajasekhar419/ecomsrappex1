package com.app.raja.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.raja.Exception.CategoryNotFoundException;
import com.app.raja.entity.Category;
import com.app.raja.service.ICategoryService;
@Controller
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	private ICategoryService service;

	@GetMapping
	public String registerString(Model model) {
		model.addAttribute("category", new Category());
		return "categoryRegister";
	}

	@PostMapping("/save")
	public String saveCategory(@ModelAttribute Category category,Model model) {
		Long id=service.saveCategory(category);
		model.addAttribute("message", "Category is created with Id:"+id);
		model.addAttribute("category", new Category());
		return "categoryRegister";

	}

	@GetMapping("/all")
	public String getAllCategory(Model model,@RequestParam(value = "message",required=false)String message) {
		List<Category> list=service.getAllCategorys();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "CategoryData";
	}

	@GetMapping("/delete")
	public String deleteCategory(@RequestParam Long id,RedirectAttributes attributes) {
		try {
			service.deleteCategory(id);
			attributes.addAttribute("message", "category deleted with Id:"+id);

		}catch (CategoryNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";

	}

	@GetMapping("/edit")
	public String showEditCategory(@RequestParam Long id,Model model,RedirectAttributes attributes) {
		String page=null;
		try {
			Category ob=service.getOneCategory(id);
			model.addAttribute("category",ob);
			page="categoryEdit";

		}catch (CategoryNotFoundException e) {

			// TODO: handle exception
			e.printStackTrace();
			attributes.addAttribute("messages", e.getMessage());
			page="redirect:all";
		}

		return page;


	}
	@PostMapping("/update")
	public String updateCategory(@ModelAttribute Category category, RedirectAttributes attributes) {
		service.updateCategory(category);
		attributes.addAttribute("message", "Category updated");
		return "redirect:all";
	}


}
