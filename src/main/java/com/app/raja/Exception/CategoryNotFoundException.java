package com.app.raja.Exception;

public class CategoryNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CategoryNotFoundException() {

	}
	public CategoryNotFoundException(String name) {
		super(name);

	}
}
